# MeetingApp Server

## Instalation

+ Requirements

Node v4.4.0

+ Install dependencies

```bash
npm install
```

## Execute the system

The system is using some features that come with the next Javascript especifications that are not available in the current version of Node, so the code is being transpiled with Babel to enable new code features, to execute the system:

```bash
npm start
```

## Quality Checks

The code is based on one of the most commont styles, Aribnb, to check that is correct:

```bash
npm run lint
```
##

##

Next version of project: https://github.com/iTermin/server